Das Ziel unserer Bewegung ist es, dass Digitalisierung und Nachhaltigkeit stärker zusammen gedacht werden. Zu Nachhaltigkeit gehört dabei nicht nur Umweltschutz und Klimaschutz, sondern auch eine starke Zivilgesellschaft, in der alle sich auf sichere Kommunikationswege verlassen können. Das ist in Deutschland durch das Persönlichkeitsrecht gesichert und, davon abgeleitet, das Recht auf informationelle Selbstbestimmung. In kurz: Ich entscheide selbst, wer meine privaten Nachrichten liest. Denn wenn ich weiß, dass staatliche Stellen oder irgendjemand sonst das auch tun können, ohne dass ich das mitkriegen würde, wird sich meine Kommunikation verändern. Vielleicht ganz unbewusst, aber ich werde mich anders verhalten.

[comment]: # Ich kann und darf mich auch daran stören, dass meine Kommunikation von Firmen analysiert wird.

Dieses Recht haben wir vor langer Zeit festgelegt und Gerichte haben das immer wieder bestätigt - als Grundrecht.

[comment]: # Bereits im Jahr 1983, lange vor der Verbreitung des Internets, hat das Bundesverfassungsgericht im Volkszählungs-Urteil den Stellenwert dieses Grundrechts bestätigt, und offensichtlich ist es heute aktueller denn je.

Natürlich können auch Kriminelle diesen Schutz nützen, um ihre Pläne abzusprechen. Aber das Argument, durch staatliche Hintertüren ließe sich das verhindern, ist völlig falsch. Verschlüsselung lässt sich schlicht nicht verbieten. 
Der Gesetzgeber kann zwar Unternehmen verpflichten, in ihre Messenger eine fehlerhafte Verschlüsselung einzubauen - denn nichts anderes bedeutet ein Generalschlüssel - aber verschlüsselte Kommunikation funktioniert auch ohne solche Dienste. WhatsApp und Signal haben das nicht erfunden, sondern nur bequem verfügbar gemacht. Das Prinzip gibt es schon sehr lange, und es gibt unzählige Software die mit diesen Methoden arbeitet. Jetzt die ganze Kryptografie verbieten zu wollen, ist genauso unsinnig wie technisch unmöglich.

Was durch staatliche Hintertüren in Messengern aber passieren wird, ist dass die vielen Menschen, die diese verbreiteten Messenger nutzen und weiter nutzen, einen Teil ihres Persönlichkeitsrechts abtreten, ohne dass dem ein Nutzen gegenübersteht. Im Gegenteil: die Erfahrung zeigt, dass diese Hintertüren in der Verschlüsselung, also absichtliche Sicherheitslücken, früher oder später missbraucht werden. Mitunter von genau den Kriminellen, die man damit versucht zu überführen. Zum Beispiel durch Erpressung mit sensiblen Informationen, die sie durch Ausnützen der Lücken erhalten.
Insgesamt bleibt daher für Kriminelle also nahezu alles beim Alten, während die Zivilgesllschaft als Ganzes einen schweren Eingriff hinnehmen muss. Das ist völlig falsch und darf so nicht passieren.

Zur Bekämpfung von Kriminalität und Terrorismus ist es dringend notwendig, dass die Sicherheitsbehörden und Geheimdienste ihre inneren Misstände beheben, und eine angemessene Ausstattung erhalten.

