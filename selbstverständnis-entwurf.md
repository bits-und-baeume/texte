# Bits und Bäume Dresden Selbstverständnis

## Präambel: Überzeugung und Ausrichtung

Wir sind der Auffassung, dass die Gestaltung der Digitalisierung dem Gemeinwohl dienen soll. Sie darf nicht einseitig auf die
Förderung einer wirtschafts- und wachstumspolitischen Agenda abzielen, sondern muss auf sozial-,  umwelt-, entwicklungs- und friedenspolitische Ziele ausgerichtet sein. Dafür muss die gesamte Gesellschaft ihren Umgang mit IT-Systemen kritisch reflektieren. Jedes Voranschreiten in Richtung einer digitalisierten Gesellschaft hat auch ökologische und soziale Folgen, die es zu bedenken gilt.

Die Gruppe Bits und Bäume Dresden befasst sich deshalb mit der Schnittmenge der Themen *Nachhaltigkeit* und *Digitalisierung*. Die inhaltliche Ausrichtung orientiert sich dabei an den [Forderungen der Bits-und-Bäume-Konferenz 2018](https://bits-und-baeume.org/forderungen/info/de).

## Ziele

1. Die Vernetzung von digitaler Bürger:innenrechte-Bewegung und allgemein technisch interessierten Menschen ("Bits") und umweltorientierter Nachhaltigkeitsbewegung ("Bäume")
2. Öffentlichkeitsarbeit für die Themen und Forderungen der Bits-und-Bäume-Konferenz
3. Systematische Sammlung und Bereitstellung von Wissen über das Themengebiet
4. Motivation und Befähigung von Individuen, zivilgesellschaftlichen Akteur:innen und öffentlichen Institutionen, zur Umsetzung der Forderungen beizutragen

## Was die Gruppe bietet

Bits und Bäume Dresden ist eine Plattform, um eigene thematisch passende Ideen umzusetzen. Dafür bietet die Gruppe eine organisatorischen und sozialen Rahmen, d.h. regelmäßige Treffen und Kommunikationsstrukturen zum Ideenaustausch und zur Gewinnung von Mitstreiter:innen. Weiterhin bietet die Marke Bits-und-Bäume eine gewisse Reichweite für öffentliche Aufmerksamkeit.

## Was die Gruppe braucht

Damit die Gruppe funktioniert, braucht es neben Infrastuktur (Mailinglisten, Webseite, Soziale Medien, ...) und deren Pflege zu aller erst Menschen. Menschen, die anderen zuhören und als Resonanzraum für Ideen zur Verfügung stehen. Menschen die im Rahmen ihrer Möglichkeiten und Interessen Hilfe anbieten und gemeinsam Projekte umsetzen.

Interessierte Menschen sind herzlich eingeladen, gemeinsam mit uns Ideen zu verwirklichen und Probleme zu lösen.


## Wie wir in der Gruppe zusammenarbeiten

Wir streben einen konstruktiven und respektvollen Dialog an, sowohl gruppenintern als auch nach außen. Wir sind grundsätzlich für alle Menschen offen. Menschenfeindliche Haltung lehnen wir ab. Innerhalb der Gruppe versuchen wir allen vorhandenen Meinungen angemessenen Raum zu geben und bei wichtigen Entscheidungen einen Konsens zu erreichen. Zur Verhinderung von ernsten Konflikten bemühen wir uns aktiv, Missverständnisse zu vermeiden und frühzeitig deeskalierend zu wirken. Kritik findet stets auf der Sachebene und nicht auf persönlicher Ebene statt. Auf problematisches Kommunikationsverhalten weisen wir in angemessener Form hin.

Mit der Zeit und Aufmerksamkeit aller Beteiligten gehen wir wertschätzend um. Wir versuchen, unsere Möglichkeiten und Grenzen realistisch einzuschätzen und uns nicht zu überfordern.

## Wie wir nach außen auftreten

Wir pflegen unseren Internet-Auftritt unter https://dresden.bits-und-baeume.org, wo wir neben allgemeinen Informationen und Veranstaltungshinweisen auch ein Blog betreiben. Beim Auftritt in sozialen Medien setzen wir vorrangig auf das freie, föderale Fediverse-Netzwerk mit Mastodon.
Erreichbar für Anfragen aller Art sind wir am besten per Mail unter `dresden ät bits-und-baeume.org`. Der Vernetzung und Zusammenarbeit mit den anderen Bits-und-Bäume-Gruppen sowie mit zivilgesellschaftlichen und aktivistischen Gruppen schenken wir dabei besondere Aufmerksamkeit. Beratung und Hilfestellung zu konkreten IT-Problemen versuchen wir im Rahmen unserer Kapazitäten zu leisten.

Wir sind grundsätzlich auch an einer Zusammenarbeit mit Institutionen und Unternehmen interessiert, sofern diese sich ebenfalls für einen dauerhaften Wandel zu nachhaltigerer Techniknutzung einsetzen und ihre Aktivitäten mit dem Geist der Präambel vereinbar sind. Das bloße Vortäuschen nachhaltiger Geschäftstätigkeit ("Greenwashing") lehnen wir ab.
