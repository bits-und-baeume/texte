---
layout: post
title: Cryptowars: wie Verschlüsselung mit Nachhaltigkeit zu tun hat
date: 2020-12-03 16:00
author: Rave
summary: eine starke Zivilgesellschaft braucht sichere Kommunikationswege
---

*Cryptowars: wie Verschlüsselung mit Nachhaltigkeit zu tun hat*.

---

Unter dem Begriff #Cryptowars werden derzeit lebhaft die [Bestrebungen des EU-Rats](https://www.heise.de/news/Terrorbekaempfung-und-Verschluesselung-EU-Rat-forciert-umstrittene-Crypto-Linie-4960069.html) zur Schwächung der Verschlüsselung in etablierten Messengern diskutiert. Ausgangspunkt war ein Mitte November [vom ORF geleaktes Papier](https://fm4.orf.at/stories/3008930).

Ausgehend von Dresden formierte sich dazu zeitnah die Kampagnen-Plattform [Privacy Is Not A Crime](https://privacyisnotacrime.eu), welche die Entwicklung zusammenfasst und über die Gefahren dieses Vorhabens aufklärt. Abschließend steht ein Statement, adressiert an die Politik, im Besonderen an das Bundesinnenministerium. Neben einer wachsenden Zahl andererer Initiativen haben auch wir dieses Statement unterzeichnet. Denn: **eine nachhaltig digitalisierte Gesellschaft braucht Technologien, sich im digitalen Raum sourverän und sicher zu bewegen.**

Im Folgenden erklären wir unsere Sicht noch einmal genauer. Dieser Text ist auch in Form in einer [Videobotschaft](https://privacyisnotacrime.eu/de#videostatements) auf der Kampagnen-Seite erschienen.

## Verschlüsselung und Nachhaltigkeit

<iframe width="560" height="315" sandbox="allow-same-origin allow-scripts allow-popups" src="https://tube.tchncs.de/videos/embed/0d3076a4-7f9d-42af-a270-f4f0b6243a7c" frameborder="0" allowfullscreen></iframe>

Das Ziel unserer Bewegung ist es, dass Digitalisierung und Nachhaltigkeit stärker zusammen gedacht werden. Zu Nachhaltigkeit gehört dabei nicht nur Umweltschutz und Klimaschutz, sondern auch eine starke Zivilgesellschaft, in der sich alle auf sichere Kommunikationswege verlassen können. Das ist in Deutschland durch das Persönlichkeitsrecht gesichert, wovon sich wiederum das Recht auf informationelle Selbstbestimmung ableitet. In kurz bedeutet dieses Recht: Wir können selbst entscheiden, wer unsere privaten Nachrichten liest. Denn wenn wir wissen, dass staatliche Stellen (oder irgendjemand sonst) das jederzeit auch tun *könnten*, wird sich unsere Kommunikation verändern. Vielleicht ganz unbewusst, aber wir werden uns anders verhalten.

Das Persönlichkeitsrecht haben wir vor langer Zeit als **Grundrecht** festgeschrieben und Gerichte haben es immer wieder bestätigt. [Im Jahr 1983](https://www.bpb.de/gesellschaft/digitales/persoenlichkeitsrechte/244837/informationelle-selbstbestimmung), lange vor der breiten Nutzung digitaler Kommunikation, hat das Bundesverfassungsgericht mit dem Volkszählungs-Urteil dieses Recht weiter präzisiert - seitdem sprechen wir von *informationeller Selbstbestimmung*.

Nun können auch Kriminelle diesen Schutz nützen, um Straftaten zu planen. Jedoch ist das Argument, durch staatliche Hintertüren ließe sich das verhindern, völlig falsch. Aus einem einfachen Grund: **Verschlüsselung lässt sich nicht verbieten**.

Der Gesetzgeber kann zwar Unternehmen verpflichten, in ihre Messenger eine fehlerhafte Verschlüsselung einzubauen - denn nichts anderes bedeutet ein Generalschlüssel - aber verschlüsselte Kommunikation funktioniert auch ohne solche Dienste. Zentrale Messenger-Plattformen wie WhatsApp und Signal haben die Kryptografie ja nicht erfunden, sondern nur bequem verfügbar gemacht. Das Prinzip und auch die technische Anwendung existieren schon viele Jahrzehnte und es gibt unzählige Software, die mit diesen Methoden arbeitet. Auch dezentrale Systeme, die für den Gesetzgeber auf diese Weise garnicht greifbar sind, zum Beispiel ein alter Bekannter: die E-Mail. Ohne sie als kriminelles Werkzeug hinstellen zu wollen: die PGP-verschlüsselte E-Mail ist ein einfaches Beispiel für einen dezentralen, software-unabhängigen vertraulichen Kommunikationsweg.

Als Reaktion darauf nun die ganze Kryptografie verbieten zu wollen, wäre genauso unsinnig wie technisch unmöglich. Kurzum: Das Vorhaben wird sein Ziel verfehlen.

Was durch staatliche Hintertüren in Messengern jedoch tatsächlich passiert: Die vielen Menschen, die sich auf verbreiteten Messengern tummeln, ohne böse Absichten zu haben, werden einen Teil ihres Persönlichkeitsrechts abtreten, ohne dass dem ein Nutzen gegenübersteht. Im Gegenteil: die Erfahrung zeigt, dass Hintertüren in der Verschlüsselungstechnik, also absichtliche Sicherheitslücken, früher oder später missbraucht werden. Und zwar mitunter genau von den Kriminellen, die man damit versuchte zu überführen. Eines von vielen Szenarios ist etwa die Erpressung mit sensiblen Informationen, die sie durch Ausnützen der Lücken erhalten. Auch die Kompromittierung von Geschäftsgeheimnissen gehört dazu.

Insgesamt bleibt daher für Kriminelle also nahezu alles beim Alten, während die Zivilgesllschaft als Ganzes einen schweren Eingriff hinnehmen muss. Das ist völlig falsch und darf so nicht passieren.

Zur Bekämpfung von Kriminalität und Terrorismus ist es dringend notwendig, dass die Sicherheitsbehörden und Geheimdienste ihre inneren Missstände beheben und eine angemessene Ausstattung erhalten.
